# packaging-tools

该仓库用于存放一些`packaging` sig 组的常用工具

## 源码反向构建工具 [source-packing](source-packing.py) 

1. 安装依赖

```sh
sudo apt-get install python3-magic git-buildpackage python3-debian pristine-tar
```

2. 使用说明

- 源码重新打包

用于对已有的debian格式的源码包重新打包，主要是用于修改源码包的格式为quilt，替换源码包的版本号修订信息等

```sh
./source-packing.py rebuild-source -h
--suite                 系列名称如yangtze，若为空将使用原有的名称
--dsc-file              源码dsc文件地址
--source-path           源码解压目录
--save-old-changes      是否保留以前旧的changelog信息
--new-revisions         是否使用新的版本修订号用于替换原有的修订版本号
```

如：假设源码包名称 sqlite3 

需要先使用`rebuild-source`子命令对源码包重新打包

（1）指定修订版本号 --new-revisions 

```sh
source-packing rebuild-source --dsc-file sqlite3_3.31.1-4kylin0.3.dsc --new-revisions ok1
```

执行成功之后，那么将生成 `sqlite3_3.31.1-ok1.dsc`

如果没有指定`--new-revisions`参数

那么对于`quilt`格式的包，那么其生成的dsc名称还是原来的

那么对于`native`格式的包，那么会默认将修订版本包设为`1`，最终生成`sqlite3_3.31.1-1.dsc`

- 反向构建出git仓库

```sh
./source-packing.py import-to-git -h
--packaging-branch  打包分支名称，默认是 openkylin/yangtze 分支名
--dsc-file          源码dsc文件地址
```

如：假设源码包名称 sqlite3 

```sh
source-packing import-to-git --dsc-file sqlite3_3.31.1-ok1.dsc --packaging-branch yhkylin/v101
```

脚本执行完成之后，会创建一个同源码名的目录，并创建好了对应的分支，如

```sh
$ git branch -a
  packaging/yhkylin/v101
  pristine-tar
  upstream
* yhkylin/v101
```

后续需要设置远端仓库之后，就可以推送到对应的托管平台上，如

```sh
git remote add origin git@gitee.com:openkylin/sqlte3.git
git push --tags && git push --all
```

- 导入不同分支代码`import-branch`

假如需要构建的包为dpkg

1. 部分分支公用一套代码

例如，v101系列和v101-2203系列共用一套代码。

> 假定v101系列的源码已经通过`源码重新打包`和`反向构建出git仓库`两个步骤在本地生成了一个git仓库，其源码目录为dpkg 


```sh
./source-packing.py import-branch --derived-branch yhkylin/v101 --packaging-branch yhkylin/v101-2203 --repo-path dpkg
```

参数说明：

**--derived-branch**：指定需要继承的来源分支

**--packaging-branch**：需要生成的目标分支

**--repo-path**：git仓库源码目录

> 注： 如果git仓库源码目录还未克隆到本地，那么可以通过指定 --git-url "git仓库url"， 如 --git-url "git@gitlab2.kylin.com:kylinos-src/base-files.git" 来将远端仓库克隆到本地

此时，会新生成`yhkylin/v101-2203`和`packaging/yhkylin/v101-2203`两个分支，后续需要推送这两个分支到远端仓库上。

2. 不同分支使用不同代码

如果是不同分支分别使用不同来源的代码，例如：

在v101系列上使用的版本是 dpkg_1.18.4kord1.7+esm1k2-1.dsc

在v101-2203系列上使用的版本是 dpkg_1.18.4kord1.7+esm1k2-10.dsc

>假定v101系列的源码已经通过2.1和2.2小节两个步骤在本地生成了一个git仓库，并推送到远端仓库。

```sh
./source-packing.py import-branch  --packaging-branch yhkylin/v101-2203 --git-url git@gitee.com:openkylin/dpkg.git --dsc-file dpkg_1.18.4kord1.7+esm1k2-10.dsc 
```

参数说明：

**--packaging-branch**：需要生成的目标分支

**--git-url**：git远端仓库url地址，用于克隆代码到本地

**--dsc-file**: 需要导入的debian源码的dsc文件

此时，会新生成`yhkylin/v101-2203`和`packaging/yhkylin/v101-2203`两个分支，后续需要推送这两个分支到远端仓库上。

## 软件源仓库依赖检查[apt-depends](apt-depends.py)

用于检查软件源仓库中软件包的依赖关系是否满足

### 安装依赖

```sh
sudo apt install python3-apt
```

### 使用方法

- 检查单个包的依赖关系

```sh
./apt-depends.py --package apt
```

- 检查整个仓库的依赖关系

```sh
./apt-depends.py
```

### 已支持的功能

- 支持单包检查（默认是candidate的版本）
- 支持单包检查时指定版本
- 支持整个仓库的所有软件包检查
- 支持`conflict`和`break`字段的依赖关系检查


### TODO

- 支持指定特定的sources.list仓库，特定架构
- 优化输出显示
- 支持反向依赖检查